//
//  main.m
//  firstGitLabDemo
//
//  Created by BidLink on 2020/3/31.
//  Copyright © 2020年 张豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
