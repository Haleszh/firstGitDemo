//
//  AppDelegate.h
//  firstGitLabDemo
//
//  Created by BidLink on 2020/3/31.
//  Copyright © 2020年 张豪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

